//
//  Friends.m
//  Parcial20152
//
//  Created by Luis F Ruiz Arroyave on 10/4/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import "Friends.h"

@implementation Friends

- (instancetype)initWithNames:(NSString*)names age:(NSString*)age career:(NSString *)career semester:(NSString *)semester photo:(NSString *)photo {
    if (self = [super init]) {
        _names = names;
        _age = age;
        _career = career;
        _semester = semester;
        _photo = photo;
    }
    return self;
}

+ (NSArray *)friends{
    
    NSArray *dataFriends = @[
                                 
                                 @{
                                     @"names":@"Juan Jose Perez",
                                     @"age":@"23",
                                     @"career":@"Ingeniería de sistemas",
                                     @"semester":@"7",
                                     @"photo":@"juan_jose_perez.jpg"
                                     
                                     },
                                 @{
                                     @"names":@"Lina Porras",
                                     @"age":@"32",
                                     @"career":@"Ingeniería Financiera",
                                     @"semester":@"9",
                                     @"photo":@"lina_porras.jpg"
                                     
                                     },
                                 @{
                                     @"names":@"Jose Trellez",
                                     @"age":@"19",
                                     @"career":@"Ingeniería Ambiental",
                                     @"semester":@"5",
                                     @"photo":@"jose_trellez.jpg"
                                     
                                     },
                                 @{
                                     @"names":@"Maria Torres",
                                     @"age":@"21",
                                     @"career":@"Ingeniería de sistemas",
                                     @"semester":@"5",
                                     @"photo":@"maria_torres.jpg"
                                     
                                     },
                                 @{
                                     @"names":@"Alvaro Uribe",
                                     @"age":@"32",
                                     @"career":@"Ingeniería de sistemas",
                                     @"semester":@"7",
                                     @"photo":@"alvaro_uribe.jpg"
                                     
                                     }
                                 
                                 ];
    Friends *friend;
    NSMutableArray *arrFriends = [NSMutableArray array];
    
    for (NSDictionary *item in dataFriends) {
        
        friend = [[Friends alloc]initWithNames:item[@"names"] age:item[@"age"] career:item[@"career"] semester:item[@"semester"] photo:item[@"photo"]];
        
        [arrFriends addObject:friend];
        
    }
    
    return arrFriends.copy;
}

@end
