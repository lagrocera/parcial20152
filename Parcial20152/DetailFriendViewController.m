//
//  DetailFriendViewController.m
//  Parcial20152
//
//  Created by S209e19 on 5/10/15.
//  Copyright (c) 2015 MRuiz. All rights reserved.
//

#import "DetailFriendViewController.h"

@interface DetailFriendViewController ()

@end

@implementation DetailFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Detail Friends";
    
    photoImageView.image = [UIImage imageNamed: self.friend.photo];
    namesLabel.text = self.friend.names;
    ageLabel.text = self.friend.age;
    careerLabel.text = self.friend.career;
    semesterLabel.text = self.friend.semester;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
