//
//  AppDelegate.h
//  Parcial20152
//
//  Created by Luis F Ruiz Arroyave on 10/4/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

