//
//  Friends.h
//  Parcial20152
//
//  Created by Luis F Ruiz Arroyave on 10/4/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Friends : NSObject

@property (strong, nonatomic) NSString *names;
@property (strong, nonatomic) NSString *age;
@property (strong, nonatomic) NSString *career;
@property (strong, nonatomic) NSString *semester;
@property (strong, nonatomic) NSString *photo;


- (instancetype)initWithNames:(NSString*)names age:(NSString*)age career:(NSString *)career semester:(NSString *)semester photo:(NSString *)photo;

+ (NSArray *)friends;

@end
