//
//  DetailFriendViewController.h
//  Parcial20152
//
//  Created by S209e19 on 5/10/15.
//  Copyright (c) 2015 MRuiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Friends.h"

@interface DetailFriendViewController : UIViewController{
    
    
    __weak IBOutlet UIImageView *photoImageView;
    __weak IBOutlet UILabel *namesLabel;
    __weak IBOutlet UILabel *ageLabel;
    __weak IBOutlet UILabel *careerLabel;
    __weak IBOutlet UILabel *semesterLabel;
}

@property(strong, nonatomic) Friends *friend;

@end
