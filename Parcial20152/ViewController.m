//
//  ViewController.m
//  Parcial20152
//
//  Created by Luis F Ruiz Arroyave on 10/4/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import "ViewController.h"
#import "Friends.h"
#import "DetailFriendViewController.h"

NSString * const CELL_IDENTIFIER = @"cellIdentifier";

@interface ViewController ()

@end

@implementation ViewController{
    NSArray *data;
    Friends *friend;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"Friends";
    
    NSLog(@"%@",[Friends friends]);
    data = [Friends friends];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return data.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CELL_IDENTIFIER];
    }
    
    friend = data[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:friend.photo];
    cell.textLabel.text = friend.names;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ años",friend.age];
    return cell;
    
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:@"detail" sender:indexPath];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NSIndexPath *)sender{
    
    DetailFriendViewController *detailVC = [segue destinationViewController];
    detailVC.friend = data[sender.row];
    
}

@end
